import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";

@Component({
  selector: 'app-nav',
  templateUrl: './app-nav.component.html',
  styleUrls: ['./app-nav.component.css']
})
export class AppNavComponent {

  @ViewChild('drawer') drawer;

  // TODO: Refactor based on RBA
  navLinks: Array<any> = [
    {
      title: 'Home',
      icon: 'home',
      path: '#',
    },
    {
      title: 'Risk Universe',
      icon: 'check_circle',
      path: '#',
    },
    {
      title: 'Audit Jobs',
      icon: 'folder',
      path: '#',
    },
    {
      title: 'Workforce',
      icon: 'people',
      path: '#',
    },
    {
      title: 'Dashboard',
      icon: 'dashboard',
      path: '#',
    },
    {
      title: 'Administration',
      icon: 'settings',
      path: 'admin',
    }
  ];
  // TODO: Get custom links from Firebase
  otherLinks: Array<any> = [
    {
      title: 'Resource Link',
      icon: 'open_in_new',
      path: '#'
    },
    {
      title: 'Resource Link',
      icon: 'open_in_new',
      path: '#'
    },
    {
      title: 'Resource Link',
      icon: 'open_in_new',
      path: '#'
    },
    {
      title: 'Resource Link',
      icon: 'open_in_new',
      path: '#'
    },
  ];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router
  ) {}

  navClick(path: string){

    this.router.navigateByUrl(path)
      .then(() => {

        // Check if drawer is active
        if (this.drawer.mode === 'over' && this.drawer.opened) {
          // Hide the drawer
          this.drawer.toggle();
        }
        
        // TODO: Remove hover in the hamburger stack icon

      })

  }

}


