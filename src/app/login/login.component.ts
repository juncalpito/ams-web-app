import { Component, OnInit } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    public afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
  }

  login() {

    const googleAuth = new auth.GoogleAuthProvider()

    // Limit the authentication popup to globe
    // This should be equally enforced in Firebase Security
    googleAuth.setCustomParameters({
      hd: 'globe.com.ph'
    });

    this.afAuth.auth.signInWithPopup(googleAuth);

  }

  logout() {
    this.afAuth.auth.signOut();
  }

}
